# Makefile for Tech Squares class lessons.
# Stephen Gildea, January 1998
# $Id: Makefile,v 1.12 2015/11/09 00:32:42 kchen Exp $
# This Makefile tested with GNU make

LESSONSRCS = lesson1.tex lesson2.tex lesson3.tex lesson4.tex \
             lesson5.tex lesson6.tex lesson7.tex lesson8.tex \
             lesson9.tex lesson10.tex lesson11.tex \
             lesson12.tex lesson13.tex


ALLSRCS = $(LESSONSRCS) formations.tex contents.tex lesson-index.tex
OTHERHANDOUTS = class-overview.tex how-to-succeed.tex recovery-skills.tex

ALL_PS = $(ALLSRCS:.tex=.PS)

all: all-pdf all-html otherhandouts

all-dvi: $(ALLSRCS:.tex=.dvi)
all-ps: $(ALL_PS)
all-pdf: $(ALLSRCS:.tex=.pdf) lessons-all.pdf
all-html: $(ALLSRCS:.tex=.html)
otherhandouts: $(OTHERHANDOUTS:.tex=.pdf)

.SUFFIXES: .tex .pdf .html

 LATEX = pdflatex
 LATEX2HTML = latex2html
 LATEX2HTML_FLAGS = -style '' -html_version 4.0 -math
 MAKEINDEX = makeindex
 PDFJOIN = pdfjam --fitpaper true --rotateoversize true --suffix joined

 ATHRUN := $(shell test -x /usr/athena/bin/athrun && echo "athrun")
 ATHRUN_TS :=  $(if $(ATHRUN), $(ATHRUN) tech-squares)

 TEXLIBDIR := $(shell [ -d /mit/tech-squares/lib/tex/macros ] || \
                      [ -d ../../lib/tex/macros ] && \
                    echo /mit/tech-squares/lib/tex/macros:../../lib/tex/macros)

ifneq "$(TEXLIBDIR)" ""
 LATEX_ENV=TEXINPUTS=.:$(TEXLIBDIR):
endif

%.pdf %.toc %.idx: %.tex
	$(LATEX_ENV) $(LATEX) $<

.tex.html:
	$(LATEX_ENV) $(DVIPS_ENV) $(LATEX2HTML) $(LATEX2HTML_FLAGS) $< && \
	$(ATHRUN_TS) ts-html-convert lessons/ $*/$@ > $@
	rm -r $*/

$(OTHERHANDOUTS:.tex=.pdf): tslib.sty
$(ALLSRCS:.tex=.pdf): classdefs.cls
$(ALLSRCS:.tex=.html): classdefs.perl

contents.html: contents-inc.tex lesson-index.ts

# latex2html runs texexpand on contents.tex.  texexpand uses perl's -r
# test ("File is readable by effective uid/gid") on contents-inc.tex,
# which can incorrectly fail in AFS.
contents-inc.tex: $(LESSONSRCS:.tex=.toc)
	(for f in $^ ; do echo $$f ; done ) | \
	sed -e 's/\([^ ][^ ]*\)/\\input{\1}/g' > $@
	chmod a+r $@

lesson-index.pdf lesson-index.html: lesson-index.ind

lesson-index.ind: $(LESSONSRCS:.tex=.idx)
	$(MAKEINDEX) -o $@ $^

lesson-index.pdf contents.pdf: lesson-index.ts contents-inc.tex

# We want this file to contain the write date of the newest of $(LESSONSRCS).
# If we have GNU ls, we use that to format the date.  Otherwise we
# have to use the current date, which isn't right because a "make clean"
# will reset it.
TS_FORMAT="+\datetag: <Id: lesson-index.ts 0.0 %Y/%m/%d %H:%M:%S $$LOGNAME>"
lesson-index.ts: $(LESSONSRCS)
	set +e; \
	ts=`ls -ldt --time-style=$(TS_FORMAT) $^ 2> /dev/null` \
	  && echo "$$ts" | sed -n '1s/.*\(\\datetag: <.*>\).*/\1/p' > $@ \
	  || date $(TS_FORMAT) > $@

formations_graphic.pdf:
	fig2dev -L pdf formations.fig $@

formations.pdf: formations_graphic.pdf
formations.html: formations_graphic.pdf

# Force all bookmarks to be closed.  Open bookmarks works nicely for
# a one-page document, but with a 17-page document, it's too long.
lessons-all.pdf: $(ALLSRCS:.tex=.pdf)
	$(PDFJOIN) --pdftitle "Tech Squares Class: All Lessons" --paper letter --outfile $@ $+

check-urls: $(OTHERHANDOUTS)
	cat $^ | grep -o '{[a-zA-Z]\+://[a-zA-Z.-]*[^}]\+}' | sed s/{// | sed s/}// | while read LINE; do curl -L -o /dev/null --silent --head --write-out '%{http_code} %{url_effective}\n' "$$LINE"; done


mostlyclean:
	-rm -f *.aux *.toc *.out *.log *.ilg .*.blg *.tar contents-inc.tex

clean: mostlyclean
	-rm -f *.idx *.pdf *.html *.ind *.bbl lesson-index.ts

distclean: clean
	-rm -f *~

# Make the tar files for distributing these files.

web.tar: $(ALLSRCS:.tex=.pdf) $(ALLSRCS:.tex=.html) $(OTHERHANDOUTS:.tex=.pdf) $(OTHERHANDOUTS:.tex=.html) lessons-all.pdf
	tar cf $@ $^
	@echo " ---"
	@echo "move $@ to /mit/tech-squares/www/lessons, then unpack it with this command:"
	@echo "tar xpfv $@"

install: web.tar
	tar xpfv web.tar -C /mit/tech-squares/www/lessons

DISTSRCS = README Makefile History classdefs.cls classdefs.perl

src.tar: $(DISTSRCS) $(ALLSRCS)
	tar cf $@ $^
	@echo " ---"
	@echo "move $@ to the destination, then unpack it with this command:"
	@echo "tar xpfv $@"
