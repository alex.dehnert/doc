\documentclass{article}
\usepackage[licensestyle,title]{tslib}
\usepackage{graphicx}
\usepackage{hyperref}
\addtolength{\topmargin}{-2cm}
\addtolength{\textheight}{4cm}
\addtolength{\oddsidemargin}{-1cm}
\addtolength{\evensidemargin}{-1cm}
\addtolength{\textwidth}{2cm}

\title{Recovery Skills}

\begin{document}
\maketitle{}

Even the best dancers make mistakes, so it's important to know how to fix mistakes and keep dancing. A good dancer uses a number of recovery techniques to help their square whether the square is doing well or falling to pieces. 
\section[``Fantasies Come True'': An Ideal Square]{``Fantasies Come True''\footnote{from \textit{Avenue Q}}: An Ideal Square}
To start, squares in an ideal situation have these properties: 
\begin{itemize}
\item The square has 2-fold rotational symmetry, which means that every dancer has an opposite diagonally across the square from them, who is the same distance from the center and facing the opposite direction.
\item The square is often in recognizable formations, such as waves, two-faced lines, or facing couples. 
\item All dancers are near other dancers in their square -- they have people next to them and/or in front of or behind them.
\item Whenever a call asks a dancer to work with another dancer, there's another dancer there to work with who wants to work with them.
\item Dancers are confident in their square's correctness. 
\end{itemize}
\section[``No One is Alone'': Preventing Mistakes]{``No One is Alone''\footnote{from \textit{Into the Woods}}: Preventing Mistakes}
Some recovery skills are pre-emptive and are used by dancers to keep squares from having problems. To maintain their squares, dancers do the following:
\begin{itemize}
\item They use handholds and precise facing directions to identify the formation. 
\item Each person has their own spot in the eight person formation -- no spots are empty and no one is without a spot. 
\item They don't panic when things deviate from perfection.
\item Dancers dance the part of the spot they're in, not the part of the spot they wish they were in. 
\item Dancers work together and wait a second for others to catch up.
\item When damage occurs, dancers repair it in a way that keeps their square dancing.
\end{itemize}
\section[``Stay With Me'': Addressing Minor Mistakes]{``Stay With Me''\footnote{from \textit{Into the Woods}}: Addressing Minor Mistakes}
Sometimes things go wrong, and it's important to know how to work with others in your square to minimize the damage and keep the problem from escalating. \\

Frequently, dancers may be unsure if they are in the right place at the end of a call. In that situation, the dancer can do these things:
\begin{itemize}
\item Wait in that position (it's better to have the wrong dancer in a spot than no dancer).
\item Check formation symmetry -- if the others dancers are in waves, they should probably face to be in waves as well.
\item Pay attention to signals from the caller or other dancers.
\end{itemize} 
Dancers and callers may use a variety of signals such as the following:
\begin{itemize}
\item Dancers may give hand gestures to tell nearby dancers to turn around, or to confirm that they are facing the right direction. 
\item Dancers may also give verbal signals such as ``You're with me'', ``turn around'', or ``facing lines''. 
\item The caller may give explicit instructions about the formation such as ``You have right-hand waves, boys on the end''
\item The next call may give implicit hints about the formation -- for example, ``couples circulate'' implies that dancers are facing the same way as their partner.  
\end{itemize}
Frequently, dancers may know they are in the right place, but not know the next call. In this case, dancers should do the following:
\begin{itemize}
\item Relax handholds with adjacent dancers. It is sometimes easy to panic and hold on to anyone handy for security, but this interferes with other dancers doing their parts of the call.
\item Look confused. This will help other dancers know that you don't know what you're doing. 
\item Move forward slowly, being careful to stay in the square. Most calls start with forward movement, so this is a good bet of how to start.
\item  As in other cases of confusion, look for people who might be trying to work with you, and listen for people telling you what to do.
\item At the same time, stay out of the way of other dancers, and try not to cut into another dancer's spot.
\end{itemize}
What about if you're not the person who's confused? You can still help them. If a person near you is confused or facing the wrong direction, you can help them by giving them signals (such as those listed above) about what they should do. If you do talk to them, be quiet and keep comments short, so they can still listen to the caller. 
\section[``Wish I Were Here'': Recovering the Formation]{``Wish I Were Here''\footnote{from \textit{Next to Normal}}: Recovering the Formation}
Sometimes, despite the efforts of the dancers, the square starts to get worse. \\

One common situation is to have a formation without all the dancers in it. In this case, there are some dancers who are either wandering around, or in the wrong formation. If you're one of those dancers, 
\begin{itemize}
\item Look at the formation the rest of the square has, and if needed peek at the formations of other squares. 
\item Find the nearest open spot in the formation and go stand in it. 
\item Use symmetry or hints from other dancers to determine your facing direction.
\item If you can't identify the formation or can't get to the open spot, wait and watch for the spot to become obvious. The square may get into a more identifiable formation soon.
\end{itemize}
\bigskip
If you're one of the people in the square with holes, you can help other dancers find spots. 
\begin{itemize}
\item Make clear where the holes are by leaving empty spots empty, and not leaving room anywhere else. 
\item If you're adjacent to an empty spot, point at it.
\item If there's someone wandering nearby who's coming to the fill the spot, take their hand or wait for them and do the next call with them. 
\item  If you're doing a call and someone you should be working with is missing, do your part of the call as best you can without them, and if possible keep track of the open spot. For instance, if you're a center on couples circulate and the end spot next to you is empty; you can do the call and then point to the empty spot that is still next to you. 
\end{itemize}

When mistakes start to occur, it's important for a square to keep dancing. The square's continued dancing is more important than any one dancer being right. Even if you don't think you're wrong, if all the rest of the square thinks one thing, go along with them, even if they might be wrong. Similarly, if you end up in a spot that you know is wrong, just keep dancing. Even if you end up in the spot of the wrong gender, keep going and be prepared to adjust. Trying to switch dancers will usually cause the square to break more. 

\section[``Pandemonium'':  When All Else Fails]{``Pandemonium''\footnote{from \textit{The 25th Annual Putnam County Spelling Bee}.}:  When All Else Fails}
Sometimes a square isn't able to recover using the previous techniques.\\

If a few dancers are still dancing, and know the current formation, they can say what it is, and the other dancers can all make that formation and everyone can keep dancing. For example, the dancers who are still up might say ``make right hand waves'' and the other dancers would find a spot in the formation so that they have right hand waves and continue dancing. \\

If that doesn't work, there's one technique left that can be used as a last resort. The technique called ``Making Lines'' is very useful for squares that are completely down as it allows them to dance some of the sequence instead of waiting for the other squares to resolve. However, it should only be used in cases when most of the square is unable to keep dancing. In this case, dancers go to their original home positions. Then the head girls join hands with their corners and the sides slide over to the right as the heads slide toward their spots. Dancers stop moving when they have facing lines, and wait for the caller to say ``Up to the middle and back'' or otherwise indicate the formation is normal facing lines. When the caller sees a square waiting in facing lines, the caller will try to maneuver the squares into facing lines, then say ``Up to the middle and back'' to indicate that the broken square can rejoin. 
\section[``Better Than Before'': Final Notes]{``Better Than Before''\footnote{from \textit{Next to Normal}}: Final Notes}
Now you've learned some ways to keep your square dancing as much as possible during a sequence. However, some people may have gotten switched. When the caller gets dancers home (for example, Promenade Home), dancers should go back to their original home position.



\end{document}
